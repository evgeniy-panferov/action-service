package com.panferov.admitadpublisher.model.admitad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CouponDto {

    private Long id;

    private String name;

    private String status;

    @JsonProperty(value = "campaign")
    private PartnerDto partner;

    private String description;

    private List<String> regions;

    private String discount;

    private String species;

    private String promocode;

    @JsonProperty(value = "frameset_link")
    private String framesetLink;

    @JsonProperty(value = "goto_link")
    private String gotoLink;

    @JsonProperty(value = "short_name")
    private String shortName;

    @JsonProperty(value = "date_start")
    private LocalDateTime dateStart;

    @JsonProperty(value = "date_end")
    private LocalDateTime dateEnd;

    @JsonProperty(value = "image")
    private String image;

}
