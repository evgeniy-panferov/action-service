package com.panferov.admitadpublisher.model.admitad;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Website{

    @JsonProperty(value = "id")
    private Long admitadId;

    private String status;

    private String name;

}
