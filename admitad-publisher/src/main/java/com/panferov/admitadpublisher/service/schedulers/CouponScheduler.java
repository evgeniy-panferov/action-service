package com.panferov.admitadpublisher.service.schedulers;


import com.panferov.admitadpublisher.config.CouponSchedulerConfig;
import com.panferov.admitadpublisher.service.publishers.CouponPublisher;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import java.util.concurrent.ScheduledExecutorService;

@Service
@Slf4j
@RequiredArgsConstructor
public class CouponScheduler {

    private final CouponSchedulerConfig config;
    private final CouponPublisher couponPublisher;
    private ThreadPoolTaskScheduler scheduler;

    public ScheduledExecutorService run() {
        log.info("Coupons Update had been start");
        scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(config.getPoolSize());
        scheduler.setThreadNamePrefix(config.getPrefix());
        scheduler.setWaitForTasksToCompleteOnShutdown(true);
        scheduler.setAwaitTerminationSeconds(config.getAwaitTerminationSec());
        scheduler.initialize();

        var couponCron = new CronTrigger(config.getCouponCron());
        scheduler.schedule(couponPublisher::publish, couponCron);
        return scheduler.getScheduledExecutor();
    }
}
