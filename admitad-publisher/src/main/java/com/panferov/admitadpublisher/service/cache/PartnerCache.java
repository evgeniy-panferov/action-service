package com.panferov.admitadpublisher.service.cache;

import com.panferov.admitadpublisher.model.admitad.PartnerDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
@Scope("singleton")
@RequiredArgsConstructor
public class PartnerCache {

    private final Map<Long, PartnerDto> cache = new ConcurrentHashMap<>();

    public PartnerDto put(Long id, PartnerDto value) {
        return cache.putIfAbsent(id, value);
    }


    public PartnerDto get(Long id) {
        return cache.get(id);
    }

    public List<PartnerDto> getPartners() {
        return new ArrayList<>(cache.values());
    }

    public void clear() {
        log.info("Cache has been cleared");
        cache.clear();
    }
}
