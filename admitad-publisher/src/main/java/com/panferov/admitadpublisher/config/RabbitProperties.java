package com.panferov.admitadpublisher.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class RabbitProperties {

    @Value("${rabbit.user}")
    private String user;

    @Value("${rabbit.password}")
    private String password;

    @Value("${rabbit.backend.queue.coupon}")
    private String backendCouponQueue;

    @Value("${rabbit.telegram.queue.coupon}")
    private String telegramCouponQueue;

    @Value("${rabbit.backend.queue.partner}")
    private String backendPartnerQueue;

    @Value("${rabbit.telegram.queue.partner}")
    private String telegramPartnerQueue;

    @Value("${rabbit.exchange.partner}")
    private String partnerExchange;

    @Value("${rabbit.exchange.coupon}")
    private String couponExchange;
}
