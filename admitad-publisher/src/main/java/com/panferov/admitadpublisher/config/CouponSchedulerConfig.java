package com.panferov.admitadpublisher.config;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
@Component
public class CouponSchedulerConfig {

    private int poolSize = 1;
    private String prefix = "CouponScheduler";
    private int awaitTerminationSec = 300;
    private String couponCron = "0 0 */4 * * *";
  //  Only Test cron
   // private String couponCron = "* */2 * * * *";

}
