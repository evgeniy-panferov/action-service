package com.actionservice.http;

import com.actionservice.model.dto.CategoryDto;
import com.actionservice.service.CategoryService;
import com.actionservice.view.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
public class CategoryCtrl {

    private final CategoryService categoryService;

    @GetMapping
    public ResponseEntity<ApiResponse<Set<CategoryDto>>> getCoupons() {
        log.info("CategoryCtrl getCoupons");
        Set<CategoryDto> categories = categoryService.findAll();
        return ResponseEntity.ok(ApiResponse.ok(categories));
    }

}
