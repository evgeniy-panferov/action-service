package com.actionservice.http.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Request {
    private String word;
}
