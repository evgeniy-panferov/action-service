package com.actionservice.model.dto.queue;

import lombok.Data;

@Data
public class CategoryDto {

    private Long id;

    private String name;

    private String language;

    private PartnerDto partner;
}
