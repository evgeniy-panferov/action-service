package com.actionservice.config.rabbit;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class RabbitProperties {

    @Value("rabbit.user")
    private String user;

    @Value("rabbit.password")
    private String password;

    @Value("rabbit.coupon.queue")
    private String couponQueue;

    @Value("rabbit.partner.queue")
    private String partnerQueue;

    @Value("rabbit.coupon.key")
    private String couponKey;

    @Value("rabbit.partner.key")
    private String partnerKey;

    @Value("rabbit.exchange")
    private String exchange;


}
