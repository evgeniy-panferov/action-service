package com.actionservice.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Slf4j
@Service
public class SVGToJpegConverter {

    public String transcodeImage(String imageUrlStr, String name) {
        String image64 = null;
        try (
                InputStream inputStream = new URL(imageUrlStr).openStream();
        ) {
            if (imageUrlStr.contains(".svg")) {
                image64 = transcodeSVGToBufferedImage(imageUrlStr, name);
            } else {
                image64 = Base64.getEncoder().encodeToString(IOUtils.toByteArray(inputStream));
            }
        } catch (IOException e) {
            log.error("Error: SVGToJpegConverter transcodeImage - {}", e.getMessage());
        }
        return image64;
    }

    @SneakyThrows
    private String transcodeSVGToBufferedImage(String imageUrl, String name) {
        //Step -1: We read the input SVG document into Transcoder Input
        //We use Java NIO for this purpose
        OutputStream pngOstream = null;
        String image64 = null;
        File file = File.createTempFile(name, ".jpeg");
        log.info("SVGToJpegConverter transcodeSVGToBufferedImage - {}", imageUrl);
        try {
            InputStream inputStream = new URL(imageUrl).openStream();
            TranscoderInput inputSvgImage = new TranscoderInput(inputStream);
            //Step-2: Define OutputStream to PNG Image and attach to TranscoderOutput
            pngOstream = new FileOutputStream(file);
            TranscoderOutput outputPngImage = new TranscoderOutput(pngOstream);
            // Step-3: Create PNGTranscoder and define hints if required
            JPEGTranscoder myConverter = new JPEGTranscoder();
            myConverter.addTranscodingHint(JPEGTranscoder.KEY_QUALITY, new Float(1.0));
            // Step-4: Convert and Write output
            myConverter.transcode(inputSvgImage, outputPngImage);
            // Step 5- close / flush Output Stream
            pngOstream.flush();
            pngOstream.close();
            image64 = Base64.getEncoder().encodeToString(IOUtils.toByteArray(new FileInputStream(file)));
            file.delete();
            return image64;
        } catch (TranscoderException | IOException e) {
            log.error("Error: SVGToJpegConverter transcodeSVGToBufferedImage - {}", e.getMessage());
            file.delete();
        } finally {
            pngOstream.flush();
            pngOstream.close();
            file.delete();
        }
        file.delete();
        return image64;
    }
}
