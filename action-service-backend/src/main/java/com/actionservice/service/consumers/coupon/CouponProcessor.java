package com.actionservice.service.consumers.coupon;


import com.actionservice.model.Category;
import com.actionservice.model.Coupon;
import com.actionservice.model.Partner;
import com.actionservice.model.dto.queue.CategoryDto;
import com.actionservice.model.dto.queue.CouponDto;
import com.actionservice.model.dto.queue.PartnerDto;
import com.actionservice.service.consumers.partner.PartnerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static com.actionservice.service.consumers.partner.PartnerChangeFinder.findChange;
import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;


@Slf4j
@Service
@RequiredArgsConstructor
public class CouponProcessor implements AutoCloseable {

    private final CouponService couponService;
    private final PartnerService partnerService;
    private final ExecutorService service = Executors.newFixedThreadPool(5);

    public void process(List<CouponDto> couponsForPartner) {
        service.execute(() -> single(couponsForPartner));
    }

    public void single(List<CouponDto> external) {

        if (CollectionUtils.isEmpty(external)) {
            return;
        }

        Map<Long, Coupon> externalCouponByAdmitadId = external.stream()
                .collect(Collectors.toMap(CouponDto::getId, CouponProcessor::convert));

        List<Coupon> back = couponService.findByAdmitadIds(externalCouponByAdmitadId.keySet());

        Partner changedPartner = findPartnerChange(external, back);
        Partner savedPartner = partnerService.save(changedPartner);

        List<Coupon> update = new ArrayList<>();
        for (Coupon coupon : back) {

            Long admitadId = coupon.getAdmitadId();
            Coupon externalCoupon = externalCouponByAdmitadId.remove(admitadId);
            externalCoupon.setPartner(savedPartner);

            Coupon changedCoupon = CouponChangeFinder.findChange(coupon, externalCoupon);

            if (CouponChangeFinder.isDifferent(coupon, externalCoupon)) {
                update.add(changedCoupon);
            }
        }

        List<Coupon> merged = externalCouponByAdmitadId.values()
                .stream()
                .peek(coupon -> coupon.setPartner(savedPartner))
                .collect(Collectors.toList());

        List<Coupon> updated = couponService.saveAll(update);
        List<Coupon> saved = couponService.saveAll(merged);

        log.info("Update coupons - {}", updated);
        log.info("Saved news coupons - {}", saved);
    }

    private Partner findPartnerChange(List<CouponDto> external, List<Coupon> back) {
        Partner result = convert(external.get(0).getPartner());
        Long admitadId = result.getAdmitadId();

        Partner partner = partnerService.findByAdmitadId(admitadId).orElse(null);

        if (partner != null) {
            result.setId(partner.getId());
        }

        if (isNotEmpty(back)) {
            Partner backPartner = back.get(0).getPartner();
            result = findChange(backPartner, result);
            result.setId(backPartner.getId());
        }

        return result;
    }

    private static Coupon convert(CouponDto dto) {
        var coupon = new Coupon();
        coupon.setAdmitadId(dto.getId());
        coupon.setName(dto.getName());
        coupon.setStatus(dto.getStatus());
        coupon.setDescription(dto.getDescription());
        coupon.setRegions(dto.getRegions());
        coupon.setPartner(convert(dto.getPartner()));
        coupon.setDiscount(dto.getDiscount());
        coupon.setSpecies(dto.getSpecies());
        coupon.setPromocode(dto.getPromocode());
        coupon.setFramesetLink(dto.getFramesetLink());
        coupon.setGotoLink(dto.getGotoLink());
        coupon.setShortName(dto.getShortName());
        coupon.setDateStart(dto.getDateStart());
        coupon.setDateEnd(dto.getDateEnd());
        coupon.setImageUrl(dto.getImage());
        return coupon;
    }

    private static Partner convert(PartnerDto dto) {
        var partner = new Partner();
        partner.setAdmitadId(dto.getId());
        partner.setName(dto.getName());
        partner.setImageUrl(dto.getImage());

        partner.setCategories(dto.getCategories()
                .stream()
                .map(categoryDto -> convert(categoryDto, partner))
                .collect(Collectors.toSet()));

        partner.setDescription(dto.getDescription());
        partner.setExclusive(dto.getExclusive());
        return partner;
    }

    private static Category convert(CategoryDto admCategory, Partner partner) {
        var category = new Category();
        category.setAdmitadId(admCategory.getId());
        category.setName(admCategory.getName());
        category.setPartner(partner);
        category.setLanguage(admCategory.getLanguage());
        return category;
    }

    @Override
    public void close() throws Exception {
        service.shutdown();
    }
}
