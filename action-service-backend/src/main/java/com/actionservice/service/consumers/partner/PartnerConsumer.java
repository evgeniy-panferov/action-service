package com.actionservice.service.consumers.partner;

import com.actionservice.model.dto.queue.PartnerDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class PartnerConsumer {

    private final PartnerProcessor processor;

    @RabbitListener(queues = "${rabbit.telegram.queue.partner}")
    public void consume(List<PartnerDto> partners){
        log.info("Have been consume partners - {}", partners);
        processor.process(partners);
    }
}
