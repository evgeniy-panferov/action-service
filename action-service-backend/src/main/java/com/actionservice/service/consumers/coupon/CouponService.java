package com.actionservice.service.consumers.coupon;


import com.actionservice.http.request.Request;
import com.actionservice.model.Category;
import com.actionservice.model.Coupon;
import com.actionservice.model.Partner;
import com.actionservice.model.dto.CategoryDto;
import com.actionservice.model.dto.CouponDto;
import com.actionservice.model.dto.PartnerDto;
import com.actionservice.repository.CouponRepository;
import com.actionservice.service.search.CouponSearch;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class CouponService {

    private final CouponRepository couponRepo;
    private final CouponSearch searchService;

    public Long save(Coupon coupon) {
        return couponRepo.save(coupon).getId();
    }

    public List<Coupon> saveAll(Collection<Coupon> coupons) {
        return couponRepo.saveAll(coupons);
    }

    public List<Coupon> findByAdmitadIds(Collection<Long> ids) {
        return couponRepo.findByAdmitadIds(ids);
    }

    public Page<CouponDto> findAllAtTheMoment(Pageable pageable){
        Page<Coupon> allAtTheMoment = couponRepo.findAllAtTheMoment(LocalDateTime.now(), pageable);

        List<CouponDto> collect = allAtTheMoment.get()
                .map(CouponService::convert)
                .collect(Collectors.toList());

        return new PageImpl<>(collect);
    }

    public List<CouponDto> findCouponByPartnerId(Long partnerId){
        return couponRepo.findCouponByPartnerId(partnerId).stream()
                .map(CouponService::convert)
                .collect(Collectors.toList());
    }

    public CouponDto findCouponById(Long id){
        return convert(couponRepo.findById(id).get());
    }

    public List<CouponDto> search(Request request){
        return searchService.search(request).stream()
                .map(CouponService::convert)
                .collect(Collectors.toList());
    }


    private static CouponDto convert(Coupon coupon) {
        var dto = new CouponDto();
        dto.setId(coupon.getId());
        dto.setName(coupon.getName());
        dto.setStatus(coupon.getStatus());
        dto.setDescription(coupon.getDescription());
        dto.setRegions(coupon.getRegions());
        dto.setPartner(convert(coupon.getPartner()));
        dto.setDiscount(coupon.getDiscount());
        dto.setSpecies(coupon.getSpecies());
        dto.setPromocode(coupon.getPromocode());
        dto.setFramesetLink(coupon.getFramesetLink());
        dto.setGotoLink(coupon.getGotoLink());
        dto.setShortName(coupon.getShortName());
        dto.setDateStart(coupon.getDateStart());
        dto.setDateEnd(coupon.getDateEnd());
        dto.setImageUrl(coupon.getImageUrl());
        return dto;
    }

    private static PartnerDto convert(Partner partner) {
        var dto = new PartnerDto();
        dto.setAdmitadId(partner.getId());
        dto.setName(partner.getName());
        dto.setImageUrl(partner.getImageUrl());

        dto.setCategories(partner
                .getCategories()
                .stream()
                .map(category -> convert(category, dto))
                .collect(Collectors.toSet())
        );

        dto.setDescription(partner.getDescription());
        dto.setExclusive(partner.getExclusive());
        return dto;
    }

    private static CategoryDto convert(Category category, PartnerDto partnerDto){
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(category.getId());
        categoryDto.setAdmitadId(category.getAdmitadId());
        categoryDto.setName(category.getName());
        categoryDto.setLanguage(category.getLanguage());
        categoryDto.setPartner(partnerDto);
        return categoryDto;
    }

}
