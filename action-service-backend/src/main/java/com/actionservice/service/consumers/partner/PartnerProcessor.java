package com.actionservice.service.consumers.partner;

import com.actionservice.model.Category;
import com.actionservice.model.Partner;
import com.actionservice.model.dto.queue.CategoryDto;
import com.actionservice.model.dto.queue.PartnerDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static com.actionservice.service.consumers.partner.PartnerChangeFinder.isDifferent;


@Slf4j
@Service
@RequiredArgsConstructor
public class PartnerProcessor implements AutoCloseable {


    private final PartnerService partnerService;
    private final ExecutorService service = Executors.newFixedThreadPool(5);

    public void process(List<PartnerDto> partners) {
        service.execute(() -> single(partners));
    }

    public void single(List<PartnerDto> external) {

        if (CollectionUtils.isEmpty(external)) {
            return;
        }

        Map<Long, Partner> externalPartnerByAdmitadId = external
                .stream()
                .collect(Collectors.toMap(PartnerDto::getId, PartnerProcessor::convert));

        List<Partner> partners = partnerService.findByAdmitadIds(externalPartnerByAdmitadId.keySet());

        List<Partner> update = new ArrayList<>();
        for (Partner partner : partners) {
            Long admitadId = partner.getAdmitadId();

            Partner externalPartner = externalPartnerByAdmitadId.remove(admitadId);

            if (externalPartner != null) {
                externalPartner.setId(partner.getId());

                if (isDifferent(partner, externalPartner)) {
                    update.add(externalPartner);
                }
            }
        }

        List<Partner> updated = partnerService.saveAll(update);
        List<Partner> saved = partnerService.saveAll(externalPartnerByAdmitadId.values());

        log.info("Update partners - {}", updated);
        log.info("Saved new partners - {}", saved);
    }


    private static Partner convert(PartnerDto dto) {
        var partner = new Partner();
        partner.setAdmitadId(dto.getId());
        partner.setName(dto.getName());
        partner.setImageUrl(dto.getImage());

        partner.setCategories(dto.getCategories()
                .stream()
                .map(categoryDto -> convert(categoryDto, partner))
                .collect(Collectors.toSet()));

        partner.setDescription(dto.getDescription());
        partner.setExclusive(dto.getExclusive());
        return partner;
    }

    private static Category convert(CategoryDto admCategory, Partner partner) {
        var category = new Category();
        category.setAdmitadId(admCategory.getId());
        category.setName(admCategory.getName());
        category.setLanguage(admCategory.getLanguage());
        category.setPartner(partner);
        return category;
    }


    @Override
    public void close() throws Exception {
        service.shutdown();
    }

}
