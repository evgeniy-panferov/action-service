package com.actionservice.service;


import com.actionservice.model.Category;
import com.actionservice.model.dto.CategoryDto;
import com.actionservice.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class CategoryService {

    private final CategoryRepository categoryRepo;

    public Category findCoupons(Long id) {
        return categoryRepo.findById(id).orElseThrow(RuntimeException::new);
    }

    public Set<CategoryDto> findAll() {
        return categoryRepo.findAll()
                .stream()
                .map(CategoryService::convert)
                .collect(Collectors.toSet());
    }

    public List<Category> getCategoriesForCurrentCoupon(){
        return categoryRepo.getCategoriesForCurrentCoupon();
    }

    public void save(Category category) {
        categoryRepo.save(category);
    }

    public void saveAll(List<Category> categories) {
        categoryRepo.saveAll(categories);
    }

    private static CategoryDto convert(Category category){
        CategoryDto dto = new CategoryDto();
        dto.setId(category.getId());
        dto.setAdmitadId(category.getAdmitadId());
        dto.setName(category.getName());
        dto.setLanguage(category.getLanguage());
        return dto;
    }
}
