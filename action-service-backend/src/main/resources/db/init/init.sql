START TRANSACTION;
DROP SCHEMA IF EXISTS tg-discount CASCADE;
CREATE SCHEMA tg-discount;
CREATE ROLE dbuser WITH
    PASSWORD 'changeme';
GRANT SELECT ON ALL TABLES IN SCHEMA tg-discount TO PUBLIC;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA tg-discount TO dbuser;
COMMIT;