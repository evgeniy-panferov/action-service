import React, { useEffect } from 'react';
import classes from "./DetailLocation.module.css";
import { Link, useParams } from "react-router-dom";
import { url } from '../Url/Url';
import axios from 'axios';
import { useState } from 'react';

export default function DetailLocation() {
    const { id } = useParams();
    const [data, setData] = useState([]);
    const [exact, setExact] = useState(false);
    const payload = data?.payload;

    useEffect(() => {
        if (Number(id)) {
            axios.get(url() + `coupons/partner/${id}`)
                .then(res => {
                    setData(res.data)
                    if (res.data) {
                        setExact(true);
                    }
                });
        }
    }, []);

    return (
        <div className={classes.location}>
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className={classes.location_row}>
                            <Link to="/">Промокоды</Link>
                            <span>{`>`}</span>
                            <Link to="/">Магазины</Link>
                            <span>{`>`}</span>
                            <Link className={classes.active} to="/">{exact ? payload[0]?.partner?.name : ""}</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
