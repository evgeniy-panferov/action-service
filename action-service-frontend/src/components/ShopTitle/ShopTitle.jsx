import React from 'react';
import classes from "./ShopTitle.module.css";

export default function ShopTitle() {
    return (
        <div className="row">
            <div className="col-md-12 col-xs-12 col-sm-12">
                <div className={classes.Title}>
                    <h1>"Cкидки и кэшбэк с покупок!</h1>
                </div>
            </div>
        </div>
    )
}
