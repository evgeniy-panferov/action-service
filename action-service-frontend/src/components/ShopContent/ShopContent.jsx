 import React, { useState } from 'react';
 import classes from "./ShopContent.module.css";
 import { Link, useNavigate } from 'react-router-dom';
 import { url } from '../Url/Url';
 import { CgChevronLeft, CgChevronRight } from "react-icons/cg";
 import { Button } from '@mui/material';
 import { useEffect } from 'react';
 import axios from 'axios';
 import CircularProgress from '@mui/material/CircularProgress';

 export default function ShopContent() {
     const [page, setPage] = useState(1);
     const [loading, setLoading] = useState(true);
     const [payload, setPayload] = useState([]);
     const [fetching, setFetching] = useState(true);

     useEffect(() => {
         if(fetching) {
             axios.get(url() + `partners?pageSize=12&pageNumber=${page}`)
                 .then(res => {
                     const response = res.data;
                     setPayload([...payload, ...response?.payload?.content]);
                     setPage(page + 1);
                     if (response?.payload?.content) {
                         setLoading(false);
                     }
                 })
                 .finally(()=>setFetching(false))
         }
     }, [fetching]);
     useEffect(()=> {
         document.addEventListener('scroll', scrollHandler)
         return function (){
             document.removeEventListener("scroll", scrollHandler)
         }
     }, [])

     const scrollHandler = (e) => {
         if (e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight) < 100) {
             setFetching(true)
             console.log('scroll')
         }
     }

     return (
         <div className="row">
                 <div className={classes.shop_row}>
                     {payload.map(item=>
                         <div className={classes.card} key={item.id}>
                             <Link to={"/" + item?.id}>
                                 <img src={item?.imageUrl} alt="img" />
                                 <p>{item?.name}</p>
                             </Link>
                         </div>
                     )}
                 </div>
             {
                 loading ? <div className={classes.loader}>
                     <CircularProgress color="inherit" />
                 </div> : ""
             }
         </div>
     )
 }