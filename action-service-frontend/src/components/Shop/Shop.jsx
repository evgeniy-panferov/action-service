import React from 'react';
import ShopContent from '../ShopContent/ShopContent';
import ShopHaeader from '../ShopHaeader/ShopHaeader';
import ShopTitle from '../ShopTitle/ShopTitle';
import classes from "./Shop.module.css";

export default function Shop() {
    return (
        <>
            <div className={classes.shop_header}>
                <div className="container">
                    <ShopHaeader />
                </div>
            </div>
            <div className={classes.Shop}>
                <div className={classes.shop_title}>
                    <div className="container">
                        <ShopTitle />
                    </div>
                </div>
                <div className={classes.shop_content_bg}>
                    <div className="container">
                        <div className={classes.Shop_content}>
                            <ShopContent />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
