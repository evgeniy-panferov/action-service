import React from 'react'

export default function Loader() {
    return (
        <div className="app-main">
            <div className="app-body"></div>
        </div>
    )
}
