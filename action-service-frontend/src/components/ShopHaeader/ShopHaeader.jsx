import React, { useRef } from 'react';
import classes1 from "../DetailNavbar/DetailNavbar.module.css";
import classes from "./ShopHaeader.module.css";
import logo from "../../asstes/logo.svg";
import { FaBars } from "react-icons/fa";
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

export default function ShopHaeader() {
    const [open, setOpen] = useState(false);
    const inputRef = useRef(null);
    const navigate = useNavigate();

    const search = (e) => {
        e.preventDefault();
        navigate(`/${inputRef.current.value}`);
    }

    return (
        <div className={open ? `${classes.ShopHeader} ${classes.active}` : classes.ShopHeader}>
            <div className="row">
                <div className='col-md-7 col-sm-12 col-xs-12'>
                    <div className={classes.ShopHeader_logo}>
                        <div className={classes.bars_icon} onClick={() => setOpen(!open)}>
                            <FaBars />
                        </div>
                        <a href='/' className={classes.logo}>
                            <img src={logo} alt="logo" />
                        </a>
                        <a className={classes.shop_link} href="/">
                            Магазины
                            <span>5 694</span>
                        </a>
                    </div>
                </div>
                <div className='col-md-5 col-sm-12 col-xs-12'>
                    <div className={classes1.search_form} style={{ paddingRight: "100px" }}>
                        <form onSubmit={search}>
                            <input type="text" ref={inputRef} placeholder='Найти магазин...' name="search_coupon"/>
                            <button type="submit">Найти</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
