import React from "react";
import NavbarTop from "../components/Navbar/Navbar_top";
import Shop from "../components/Shop/Shop";
import "./Flexboxgrid/flexboxgrid.css";
import { Routes, Route } from "react-router-dom";
import DetailPage from "./DetailPage";

export default function Layout() {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <>
            <NavbarTop />
            <Shop />
          </>
        }
      />
      <Route path="/:id" element={<DetailPage />} />
    </Routes>
  );
}
