import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Detail from '../components/Detail/Detail';
import DetailLocation from '../components/DetailLocation/DetailLocation';
import DetailNavbar from '../components/DetailNavbar/DetailNavbar';
import { url } from '../components/Url/Url';
import axios from 'axios';

export default function DetailPage() {
  const { id } = useParams();
  const [payload,setPayload] = useState([]);
  const [loaded,setLoaded] = useState(true);

  useEffect(() => {
    if(Number(id)) {
      axios.get(url() + `coupons/partner/${id}`)
        .then(res => {
            const persons = res.data;
            setPayload(persons?.payload);
            if(persons?.payload) {
              setLoaded(false);
            }
        });
    } else {
      const url1 = url() + `coupons/search?word=${id}`;
      axios.get(url1)
        .then(res => {
            const persons = res.data;
            setPayload(persons?.payload);
            if(persons?.payload) {
              setLoaded(false);
            }
        });
    }
    
}, []);

  return (
    <>
        <DetailNavbar setPayload={setPayload} setLoaded={setLoaded} />
        <DetailLocation />
        <Detail payload={payload} loaded={loaded} />
    </>
  )
}
