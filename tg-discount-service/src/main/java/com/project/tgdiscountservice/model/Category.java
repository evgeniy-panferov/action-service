package com.project.tgdiscountservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Table(name = "telegram_category")
@Entity
@Getter
@Setter
public class Category extends AbstractEntity{

    private String name;

    private Long admitadId;

    private String language;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "partner_id")
    @JsonBackReference
    private Partner partner;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(name, category.name) && Objects.equals(admitadId, category.admitadId) && Objects.equals(language, category.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, admitadId, language);
    }
}
