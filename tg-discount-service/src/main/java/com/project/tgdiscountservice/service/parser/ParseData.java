package com.project.tgdiscountservice.service.parser;

import com.project.tgdiscountservice.model.inner.InnerCallBackQuery;
import com.project.tgdiscountservice.model.inner.InnerMessage;
import lombok.Data;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;

@Data
public class ParseData {
    private InnerMessage tgMessage;
    private String command = "";
    private String navigateCommand = "";
    private int index = 0;
    private String callBackData = "";
    private InnerCallBackQuery callbackQuery;
    private Long chatId = 0L;
    private String[] split;
    private String id;
    private String query;
    private InlineQuery inlineQuery;
}
