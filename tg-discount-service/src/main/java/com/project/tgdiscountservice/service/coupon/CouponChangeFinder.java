package com.project.tgdiscountservice.service.coupon;

import com.project.tgdiscountservice.model.Coupon;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CouponChangeFinder {

    public static Coupon findChange(Coupon coupon, Coupon externalCoupon) {
        int externalHash = hashGenerator(externalCoupon);
        int couponHash = hashGenerator(coupon);

        if (externalHash != couponHash) {
            return externalCoupon;
        }
        return coupon;
    }

    public static boolean isDifferent(Coupon coupon, Coupon externalCoupon) {
        int externalHash = hashGenerator(externalCoupon);
        int couponHash = hashGenerator(coupon);

        return externalHash != couponHash;
    }

    private static int hashGenerator(Coupon coupon) {
        return Objects.hash(
                coupon.getName(),
                coupon.getStatus(),
                coupon.getDescription(),
                coupon.getDiscount(),
                coupon.getPromocode(),
                coupon.getFramesetLink(),
                coupon.getGotoLink(),
                coupon.getShortName(),
                coupon.getDateStart(),
                coupon.getDateEnd(),
                coupon.getImageUrl(),
                coupon.getSpecies());
    }
}
