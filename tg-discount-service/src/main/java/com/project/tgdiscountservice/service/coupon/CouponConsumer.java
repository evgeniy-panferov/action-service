package com.project.tgdiscountservice.service.coupon;


import com.project.tgdiscountservice.model.dto.CouponDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CouponConsumer {

    private final CouponProcessor couponProcessor;

    @RabbitListener(queues = "${rabbit.telegram.queue.coupon}")
    public void consume(List<CouponDto> couponsForPartner) {
        log.info("Have been consume coupon - {}", couponsForPartner);
        couponProcessor.process(couponsForPartner);
    }
}
