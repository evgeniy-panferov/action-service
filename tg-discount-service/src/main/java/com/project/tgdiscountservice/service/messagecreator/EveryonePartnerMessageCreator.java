package com.project.tgdiscountservice.service.messagecreator;

import com.project.tgdiscountservice.model.Partner;
import com.project.tgdiscountservice.model.inner.InnerUpdate;
import com.project.tgdiscountservice.repository.PartnerRepo;
import com.project.tgdiscountservice.service.MessageSenderFacade;
import com.project.tgdiscountservice.service.parser.ParseData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.util.List;

import static com.project.tgdiscountservice.model.Emoji.MAGNIFYING_GLASS_TILTED_LEFT;
import static com.project.tgdiscountservice.model.Emoji.MAGNIFYING_GLASS_TILTED_RIGHT;
import static com.project.tgdiscountservice.util.InlineKeyboard.getNavigateCallbackKeyboard;

@Slf4j
@Service
@RequiredArgsConstructor
public class EveryonePartnerMessageCreator implements MessageCreator {

    private static final String TYPE_RESOLVER = "/shops";
    private final MessageSenderFacade messageSenderFacade;
    private final PartnerRepo partnerRepo;
    @Value("${app.host}")
    private String host;


    @Override
    public void create(InnerUpdate update, ParseData data) {
        log.info("EveryonePartnerMessageCreator prepareMessage - {}, {}", update, data);

        List<Partner> partners = partnerRepo.findAll();
        StringBuilder message;
        if (partners.isEmpty()) {
            message = new StringBuilder();
            message.append(MAGNIFYING_GLASS_TILTED_RIGHT).append("Ни чего не найдено").append(MAGNIFYING_GLASS_TILTED_LEFT);
            messageSenderFacade.sendMessage(update, message);
            return;
        }

        for (int i = 0; i < partners.size(); i++) {
            message = new StringBuilder();
            Partner partner = partners.get(i);
            String imageUrl = partner.getImageUrl();
            if (imageUrl.contains(".svg")) {
                message.append("<a href=\"").append(host).append("/pictures/partner/").append(partner.getId()).append("\">").append(" ").append("</a>");
            } else {
                message.append("<a href=\"").append(imageUrl).append("\">").append(" ").append("</a>");
            }

            message.append(partner.getName()).append("\n");
            InlineKeyboardMarkup navigateKeyboard = getNavigateCallbackKeyboard("/cp", "0", partner.getId().toString(),
                    "", "Список акций");
            messageSenderFacade.sendMessage(update, message, navigateKeyboard);
        }

    }

    @Override
    public String getCommand() {
        return TYPE_RESOLVER;
    }
}