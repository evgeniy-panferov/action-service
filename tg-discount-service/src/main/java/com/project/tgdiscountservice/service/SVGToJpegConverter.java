package com.project.tgdiscountservice.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.util.Base64;

@Slf4j
@Service
public class SVGToJpegConverter {

    @SneakyThrows
    public byte[] transcodeSVGToBufferedImage(String imageUrl) {
        //Step -1: We read the input SVG document into Transcoder Input
        //We use Java NIO for this purpose
        log.info("SVGToJpegConverter transcodeSVGToBufferedImage - {}", imageUrl);
        OutputStream pngOstream = null;
        String path = "pictures/pic.png";
        try {
            InputStream inputStream = new URL(imageUrl).openStream();
            TranscoderInput inputSvgImage = new TranscoderInput(inputStream);
            //Step-2: Define OutputStream to PNG Image and attach to TranscoderOutput
            String namePic = "pictures/pic.png";
            File file = new File(namePic);
            pngOstream = new FileOutputStream(file);
            TranscoderOutput outputPngImage = new TranscoderOutput(pngOstream);
            // Step-3: Create PNGTranscoder and define hints if required
            PNGTranscoder myConverter = new PNGTranscoder();
            // Step-4: Convert and Write output
            myConverter.transcode(inputSvgImage, outputPngImage);
            // Step 5- close / flush Output Stream
            pngOstream.flush();
            pngOstream.close();
            path = file.getAbsolutePath();
            return FileUtils.readFileToByteArray(new File(path));
        } catch (TranscoderException | IOException e) {
            log.error("Error: SVGToJpegConverter transcodeSVGToBufferedImage - {}", e.getMessage());
            e.printStackTrace();
        } finally {
            pngOstream.flush();
            pngOstream.close();
        }
        return FileUtils.readFileToByteArray(new File(path));
    }
}
