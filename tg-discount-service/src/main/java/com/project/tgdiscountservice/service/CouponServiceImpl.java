package com.project.tgdiscountservice.service;

import com.project.tgdiscountservice.model.Coupon;
import com.project.tgdiscountservice.model.dto.CouponDto;
import com.project.tgdiscountservice.repository.CouponRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class CouponServiceImpl {

    private final CouponRepo couponRepo;

    public Coupon findCoupons(Long id) {
        return couponRepo.findById(id)
                .orElseThrow(RuntimeException::new);
    }

    public void save(Coupon coupon) {
        couponRepo.save(coupon);
    }

    public List<Coupon> findAll() {
        return couponRepo.findAll();
    }


}
