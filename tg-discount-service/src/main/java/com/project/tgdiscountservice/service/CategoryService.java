package com.project.tgdiscountservice.service;

import com.project.tgdiscountservice.model.Category;
import com.project.tgdiscountservice.repository.CategoryRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class CategoryService {

    private final CategoryRepo categoryRepo;

    public Category findCoupons(Long id) {
        return categoryRepo.findById(id).orElseThrow(RuntimeException::new);
    }

    public List<Category> findAll() {
        return categoryRepo.findAll();
    }

    public List<Category> getCategoriesForCurrentCoupon(){
        return categoryRepo.getCategoriesForCurrentCoupon();
    }

    public void save(Category category) {
        categoryRepo.save(category);
    }

    public void saveAll(List<Category> categories) {
        categoryRepo.saveAll(categories);
    }
}
