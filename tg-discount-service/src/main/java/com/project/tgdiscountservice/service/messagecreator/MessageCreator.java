package com.project.tgdiscountservice.service.messagecreator;

import com.project.tgdiscountservice.model.inner.InnerUpdate;
import com.project.tgdiscountservice.service.parser.ParseData;

public interface MessageCreator {
    void create(InnerUpdate update, ParseData data);

    String getCommand();
}
