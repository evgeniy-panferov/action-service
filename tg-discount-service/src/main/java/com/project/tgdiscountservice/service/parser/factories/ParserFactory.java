package com.project.tgdiscountservice.service.parser.factories;

import com.project.tgdiscountservice.service.parser.Parser;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Getter
@Service
@RequiredArgsConstructor
public class ParserFactory {

    private Map<String, Parser> parsersByCommand;

    @Autowired
    public ParserFactory(List<Parser> parsers) {
        parsersByCommand = parsers.stream()
                .collect(Collectors.toUnmodifiableMap(Parser::getCommand, Function.identity()));
    }

    public Parser getParserByCommand(String command) {
        return parsersByCommand.get(command);
    }
}
