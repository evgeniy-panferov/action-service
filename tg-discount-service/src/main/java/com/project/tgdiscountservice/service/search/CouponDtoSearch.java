package com.project.tgdiscountservice.service.search;

import com.project.tgdiscountservice.model.Partner;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class CouponDtoSearch {

    private Long id;

    private Long admitadId;

    private String name;

    private String status;

    private Partner partner;

    private String description;

    private List<String> regions;

    private String discount;

    private String species;

    private String promocode;

    private String framesetLink;

    private String gotoLink;

    private String shortName;

    private LocalDateTime dateStart;

    private LocalDateTime dateEnd;

    private String imageUrl;

    private LocalDateTime lastUpdate;

    private LocalDateTime createDate;

    private String image64;

}
