package com.project.tgdiscountservice.service.partner;

import com.project.tgdiscountservice.model.Partner;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PartnerChangeFinder {

    public static Partner findChange(Partner partner, Partner externalPartner) {

        int partnerHash = hashGenerator(partner);
        int externalPartnerHash = hashGenerator(externalPartner);

        if (partnerHash != externalPartnerHash) {
            return externalPartner;
        }

        return partner;
    }

    public static boolean isDifferent(Partner partner, Partner externalPartner) {

        int partnerHash = hashGenerator(partner);
        int externalPartnerHash = hashGenerator(externalPartner);
        return partnerHash != externalPartnerHash;
    }


    private static int hashGenerator(Partner partner) {
        return Objects.hash(
                partner.getName(),
                partner.getImageUrl(),
                partner.getDescription(),
                partner.getCategories(),
                partner.getExclusive());
    }

}
