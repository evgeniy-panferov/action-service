package com.project.tgdiscountservice.service.parser.factories;

import com.project.tgdiscountservice.service.messagecreator.MessageCreator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MessageCreatorFactory {

    private Map<String, MessageCreator> handlersByCommand;

    @Autowired
    public MessageCreatorFactory(List<MessageCreator> handlers) {
        handlersByCommand = handlers.stream()
                .collect(Collectors.toUnmodifiableMap(MessageCreator::getCommand, Function.identity()));
    }


    public MessageCreator getCreator(String command) {
        return handlersByCommand.get(command);
    }

}
