package com.project.tgdiscountservice.repository;

import com.project.tgdiscountservice.model.Coupon;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface CouponRepo extends JpaRepository<Coupon, Long> {

    @EntityGraph(value = "Coupon[regions, partner]")
    @Query("select c from Coupon c where c.admitadId in :admitadIds")
    List<Coupon> findByAdmitadIds(@Param("admitadIds") Collection<Long> admitadIds);
}
